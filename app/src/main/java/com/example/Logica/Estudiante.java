package com.example.Logica;

public class Estudiante {
    public String id;
    public String nombre;
    public String apellido;
    public String correo;

    public Estudiante(){

    }

    public Estudiante(String id, String nombre, String apellido) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
    }
}
