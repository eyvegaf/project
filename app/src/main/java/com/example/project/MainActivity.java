package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.Logica.Estudiante;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;

public class MainActivity extends AppCompatActivity {
    private Button buttonInsertar;
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.buttonInsertar=findViewById(R.id.buttonIngreso);
        this.buttonInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertar();
            }
        });
    }

    private void insertar() {
        EditText id= findViewById(R.id.editTextId);
        EditText nombre = findViewById(R.id.editTextNombre);
        EditText apellido= findViewById(R.id.editTextApellido);

        Estudiante estudiante= new Estudiante(id.getText().toString(),nombre.getText().toString(), apellido.getText().toString());
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("estudiantes").child(id.getText().toString()).setValue(estudiante);

    }
}